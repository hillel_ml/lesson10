# ДЗ 10. Улучшение нейросетевой модели (основанной на сверточных сетях) для задачи классификации изображений

1. Используя приведенный на занятии код для классификации набора данных CIFAR, добиться улучшения качества работы модели, основанной на сверточных нейронных сетях.
2. Составить отчет о настройке модели, привести комбинацию параметров, обеспечивающих наилучшие показатели качества.
3. Привести графики изменения параметров алгоритма обучения.
4. Описать одну из популярных архитектур для работы с изображениями


Number 1: With dropout without BN
Epoch 10/10
1563/1563 [==============================] - 254s 162ms/step - loss: 1.1048 - accuracy: 0.6095 - val_loss: 1.0433 - val_accuracy: 0.6319


Number 2: With BN without dropout
Epoch 10/10
1563/1563 [==============================] - 299s 191ms/step - loss: 0.5223 - accuracy: 0.8150 - val_loss: 0.6963 - val_accuracy: 0.7572


Number 3: With BN and dropout
Epoch 10/10
1563/1563 [==============================] - 283s 181ms/step - loss: 0.7663 - accuracy: 0.7281 - val_loss: 0.7237 - val_accuracy: 0.7426




Number 4: with regularizers - l2
Epoch 1/10
1563/1563 [==============================] - ETA: 0s - loss: 7.7928 - accuracy: 0.45312022-11-24 18:01:45.779138: W tensorflow/tsl/framework/cpu_allocator_impl.cc:82] Allocation of 122880000 exceeds 10% of free system memory.
1563/1563 [==============================] - 243s 155ms/step - loss: 7.7928 - accuracy: 0.4531 - val_loss: 5.8372 - val_accuracy: 0.5232
Epoch 2/10
1563/1563 [==============================] - 237s 151ms/step - loss: 4.4932 - accuracy: 0.6025 - val_loss: 3.3975 - val_accuracy: 0.6666
Epoch 3/10
1563/1563 [==============================] - 311s 199ms/step - loss: 2.7772 - accuracy: 0.6766 - val_loss: 2.2634 - val_accuracy: 0.6880
Epoch 4/10
1563/1563 [==============================] - 315s 202ms/step - loss: 1.8606 - accuracy: 0.7185 - val_loss: 1.6218 - val_accuracy: 0.7148
Epoch 5/10
1563/1563 [==============================] - 325s 208ms/step - loss: 1.3542 - accuracy: 0.7492 - val_loss: 1.2558 - val_accuracy: 0.7383
Epoch 6/10
1563/1563 [==============================] - 346s 222ms/step - loss: 1.0767 - accuracy: 0.7710 - val_loss: 1.0613 - val_accuracy: 0.7543
Epoch 7/10
1563/1563 [==============================] - 370s 237ms/step - loss: 0.9164 - accuracy: 0.7878 - val_loss: 0.9555 - val_accuracy: 0.7639
Epoch 8/10
1563/1563 [==============================] - 307s 197ms/step - loss: 0.8126 - accuracy: 0.8075 - val_loss: 0.9865 - val_accuracy: 0.7382
Epoch 9/10
1563/1563 [==============================] - 277s 177ms/step - loss: 0.7512 - accuracy: 0.8197 - val_loss: 0.8861 - val_accuracy: 0.7783
Epoch 10/10
1563/1563 [==============================] - 323s 206ms/step - loss: 0.7094 - accuracy: 0.8348 - val_loss: 0.9473 - val_accuracy: 0.7565

Number 5: with regularizers - l2 and dropout
Epoch 1/10
1563/1563 [==============================] - 266s 168ms/step - loss: 7.9238 - accuracy: 0.4133 - val_loss: 5.7966 - val_accuracy: 0.5355
Epoch 2/10
1563/1563 [==============================] - 280s 179ms/step - loss: 4.6415 - accuracy: 0.5445 - val_loss: 3.6244 - val_accuracy: 0.5783
Epoch 3/10
1563/1563 [==============================] - 330s 211ms/step - loss: 2.9551 - accuracy: 0.6069 - val_loss: 2.3102 - val_accuracy: 0.6611
Epoch 4/10
1563/1563 [==============================] - 296s 189ms/step - loss: 2.0221 - accuracy: 0.6582 - val_loss: 1.6724 - val_accuracy: 0.6941
Epoch 5/10
1563/1563 [==============================] - 309s 198ms/step - loss: 1.5103 - accuracy: 0.6926 - val_loss: 1.3431 - val_accuracy: 0.7001
Epoch 6/10
1563/1563 [==============================] - 297s 190ms/step - loss: 1.2364 - accuracy: 0.7153 - val_loss: 1.1462 - val_accuracy: 0.7190
Epoch 7/10
1563/1563 [==============================] - 308s 197ms/step - loss: 1.0739 - accuracy: 0.7294 - val_loss: 1.0064 - val_accuracy: 0.7472
Epoch 8/10
1563/1563 [==============================] - 326s 209ms/step - loss: 0.9823 - accuracy: 0.7446 - val_loss: 0.9409 - val_accuracy: 0.7524
Epoch 9/10
1563/1563 [==============================] - 291s 186ms/step - loss: 0.9288 - accuracy: 0.7562 - val_loss: 0.9134 - val_accuracy: 0.7607
Epoch 10/10
1563/1563 [==============================] - 316s 202ms/step - loss: 0.8976 - accuracy: 0.7632 - val_loss: 0.9320 - val_accuracy: 0.7514





Number 6:
Epoch 1/10
1563/1563 [==============================] - 298s 190ms/step - loss: 5.5424 - accuracy: 0.3704 - val_loss: 4.5190 - val_accuracy: 0.3803
Epoch 2/10
1563/1563 [==============================] - 322s 206ms/step - loss: 3.3685 - accuracy: 0.5129 - val_loss: 2.6709 - val_accuracy: 0.5621
Epoch 3/10
1563/1563 [==============================] - 338s 216ms/step - loss: 2.2711 - accuracy: 0.5806 - val_loss: 2.0309 - val_accuracy: 0.5553
Epoch 4/10
1563/1563 [==============================] - 360s 231ms/step - loss: 1.6557 - accuracy: 0.6277 - val_loss: 1.3880 - val_accuracy: 0.6650
Epoch 5/10
1563/1563 [==============================] - 326s 208ms/step - loss: 1.3035 - accuracy: 0.6613 - val_loss: 1.1271 - val_accuracy: 0.6957
Epoch 6/10
1563/1563 [==============================] - 307s 196ms/step - loss: 1.1021 - accuracy: 0.6843 - val_loss: 0.9793 - val_accuracy: 0.7169
Epoch 7/10
1563/1563 [==============================] - 280s 179ms/step - loss: 0.9755 - accuracy: 0.7081 - val_loss: 0.8891 - val_accuracy: 0.7273
Epoch 8/10
1563/1563 [==============================] - 280s 179ms/step - loss: 0.8998 - accuracy: 0.7209 - val_loss: 0.9516 - val_accuracy: 0.6986
Epoch 9/10
1563/1563 [==============================] - 300s 192ms/step - loss: 0.8442 - accuracy: 0.7361 - val_loss: 0.7964 - val_accuracy: 0.7510
Epoch 10/10
1563/1563 [==============================] - 305s 195ms/step - loss: 0.8004 - accuracy: 0.7490 - val_loss: 0.8164 - val_accuracy: 0.7383

Number 7:
Epoch 1/10
1563/1563 [==============================] - 816s 520ms/step - loss: 8.5308 - accuracy: 0.4218 - val_loss: 6.2559 - val_accuracy: 0.5469
Epoch 2/10
1563/1563 [==============================] - 908s 581ms/step - loss: 4.9584 - accuracy: 0.5644 - val_loss: 3.7428 - val_accuracy: 0.6414
Epoch 3/10
1563/1563 [==============================] - 1072s 686ms/step - loss: 3.0823 - accuracy: 0.6425 - val_loss: 2.4348 - val_accuracy: 0.6818
Epoch 4/10
1563/1563 [==============================] - 994s 636ms/step - loss: 2.0765 - accuracy: 0.6883 - val_loss: 1.7431 - val_accuracy: 0.7104
Epoch 5/10
1563/1563 [==============================] - 681s 436ms/step - loss: 1.5330 - accuracy: 0.7195 - val_loss: 1.3324 - val_accuracy: 0.7403
Epoch 6/10
1563/1563 [==============================] - 693s 444ms/step - loss: 1.2287 - accuracy: 0.7417 - val_loss: 1.1000 - val_accuracy: 0.7652
Epoch 7/10
1563/1563 [==============================] - 754s 482ms/step - loss: 1.0566 - accuracy: 0.7615 - val_loss: 1.0287 - val_accuracy: 0.7594
Epoch 8/10
1563/1563 [==============================] - 700s 448ms/step - loss: 0.9562 - accuracy: 0.7752 - val_loss: 0.9341 - val_accuracy: 0.7826
Epoch 9/10
1563/1563 [==============================] - 682s 436ms/step - loss: 0.8944 - accuracy: 0.7926 - val_loss: 0.9380 - val_accuracy: 0.7731
Epoch 10/10
1563/1563 [==============================] - 684s 438ms/step - loss: 0.8507 - accuracy: 0.8025 - val_loss: 0.9055 - val_accuracy: 0.7887