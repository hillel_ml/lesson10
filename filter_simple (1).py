import cv2
import numpy as np
import matplotlib.pyplot as plt

from scipy.signal import convolve2d

img = cv2.imread('Lena.jpg')
#img = cv2.imread('1.jpg')
print(img.shape)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# print(gray.shape)
# # print(gray[100, 100])
#
# plt.imshow(gray, cmap='gray')
# plt.show()
#
mean = 0
var = 100
sigma = var ** 0.5
row, col = 256, 256
gauss = np.random.normal(mean, sigma, (row, col))
gauss = gauss.reshape(row, col)
gray_noisy = gray + gauss
# plt.imshow(gray_noisy, cmap='gray')
# plt.show()
#
# # Усредняющий фильтр
# Hm1 = np.array([[1, 1, 1],
#                [1, 1, 1],
#                [1, 1, 1]])/float(9)
# # #
# Hm2 = np.array([[0, 0, 0],
#                 [1, 0, -1],
#                 [0, 0, 0]])
#
#
# Gm = convolve2d(gray_noisy, Hm2, mode='same')
# plt.imshow(Gm, cmap='gray')
# plt.show()
# # #
# Hm3 = np.array([[0, 1, 0],
#                 [0, 0, 0],
#                 [0, -1, 0]])
#
# Gm = convolve2d(gray_noisy, Hm3, mode='same')
# plt.imshow(Gm, cmap='gray')
# plt.show()
# #
# # #
# Hg = np.zeros((20, 20))
# for i in range(20):
#     for j in range(20):
#         Hg[i, j] = np.exp(-((i-10)**2 + (j-10)**2)/10)
#
# Gm = convolve2d(gray_noisy, Hg, mode='same')
# plt.imshow(Gm, cmap='gray')
# plt.show()
# #
# #
# # # Hm = np.array([[1, 1],
# # #                [1, 1]])/float(9)
#
S1 = np.array([[1, 0, -1],
               [2, 0, -2],
               [1, 0, -1]])

S2 = np.array([[1, 2, 1],
               [0, 0, 0],
               [-1, -2, -1]])

S3 = np.array([[-1, -1, -1],
               [-1, 8, -1],
               [-1, -1, -1]])

Gm = convolve2d(gray_noisy, S1, mode='same')
Gm2 = convolve2d(gray_noisy, S2, mode='same')
Gm3 = convolve2d(gray_noisy, S3, mode='same')
plt.imshow(Gm, cmap='gray')
plt.show()

plt.imshow(Gm3, cmap='hot')
plt.show()