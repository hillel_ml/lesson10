import cv2
import numpy as np
import matplotlib.pyplot as plt

from scipy.signal import convolve2d

img = cv2.imread('lesson10/1.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

plt.imshow(gray)
plt.show()

mean = 0
var = 100
sigma = var ** 0.5
row, col = 256, 256
gauss = np.random.normal(mean, sigma, (row, col))
gauss = gauss.reshape(row, col)
gray_noisy = gray + gauss
plt.imshow(gray_noisy)
plt.show()

# Усредняющий фильтр
Hm1 = np.array([[1, 1, 1],
               [1, 1, 1],
               [1, 1, 1]])/float(9)
#
# Hm2 = np.array([[0, 0, 0],
#                 [1, 0, -1],
#                 [0, 0, 0]])
#
# Gm = convolve2d(gray_noisy, Hm2, mode='same')
# plt.imshow(Gm)
# plt.show()
#
# Hm3 = np.array([[0, 1, 0],
#                 [0, 0, 0],
#                 [0, -1, 0]])
#
# Gm = convolve2d(gray_noisy, Hm3, mode='same')
# plt.imshow(Gm)
# plt.show()

#
# Hg = np.zeros((20, 20))
# for i in range(20):
#     for j in range(20):
#         Hg[i, j] = np.exp(-((i-10)**2 + (j-10)**2)/10)


# Hm = np.array([[1, 1],
#                [1, 1]])/float(9)

S1 = np.array([[1, 0, -1],
               [2, 0, -2],
               [1, 0, -1]])

Gm = convolve2d(gray_noisy, S1, mode='same')
plt.imshow(Gm)
plt.show()